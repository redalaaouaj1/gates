<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $posts = Post::all();
        return view('posts.index',compact('posts'));
    }
    public function create()
    {
        // $users = User::all();
        return view('posts.create');
    }
    public function store(Request $request)
    {
        $user = auth()->user()->id;
        $validatedData = $request->validate([
            'title' => 'required|max:25',
            'description' => 'required'
        ]);
        $validatedData['user_id'] = $user;
        Post::create($validatedData);
        return to_route('posts.index')->with('success','La publication a été ajoutée avec success ✔');
    }
    public function edit($id)
    {
        $post = Post::findOrFail($id);
        return view('posts.edit',compact('post'));
    }
    public function update($id, Request $request)
    {
        $post = Post::findOrFail($id);
        $validatedData = $request->validate([
            'title' => 'required|max:25',
            'description' => 'required'
        ]);
        $post->update($validatedData);
        return to_route('posts.index')->with('success','La publication a été modifiée avec success ✔');
    }
    public function destroy($id)
    {
        $post = Post::find($id);
        $post->delete();
        return to_route('posts.index')->with('success','La publication a été supprimée avec success ✔');
    }
}
