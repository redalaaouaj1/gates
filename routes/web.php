<?php

use App\Http\Controllers\LoginController;
use App\Http\Controllers\PostController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::get('/login',[LoginController::class, 'form'])->name('login');
Route::post('/login',[LoginController::class, 'connect'])->name('login.connect');
Route::get('/signUp', LoginController::class . '@signUp')->name('login.signUpForm'); // different way to call the controller
Route::post('/signUp',[LoginController::class, 'register'])->name('login.register');
Route::get('/logout',[LoginController::class, 'logout'])->name('logout');

Route::resource('posts',PostController::class)->except('show');

Route::get('/', function () {
    return view('welcome');
})->name('welcome');
