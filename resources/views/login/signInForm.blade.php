@extends('layouts.app')
@section('content')

<div class="h-100">
    <div class="row d-flex align-items-center justify-content-center h-100">
        <div class="col-md-7 col-lg-5 col-xl-5 offset-xl-1">
            <form action="{{route('login.connect')}}" method="post">
                @csrf

                <div class="mb-3">
                    <label for="email" class="form-label">Email</label>
                    <input type="email" class="form-control" name="email" value="{{old('email')}}" />
                    @error('email')
                    <span class="text text-danger">{{$message}}</span>
                    @enderror
                </div>

                <div class="form-outline mb-4">
                    <label class="form-label" for="form1Example23">Password</label>
                    <input type="password" name="password" class="form-control form-control-lg" />
                </div>
                <div class="d-flex justify-content-around align-items-center mb-4">
                <button type="submit" class="btn btn-primary btn-lg btn-block">Sign in</button>
                <a href="{{route('login.signUpForm')}}" class="btn btn-secondary btn-lg btn-block">Create account</a>
            </form>
        </div>
    </div>
</div>

@endsection