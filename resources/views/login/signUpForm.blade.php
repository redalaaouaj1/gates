@extends('layouts.app')
@section('content')

<div class="h-100">
    <div class="row d-flex align-items-center justify-content-center h-100">
        <div class="col-md-7 col-lg-5 col-xl-5 offset-xl-1">
            <form action="{{route('login.register')}}" method="post">
                @csrf

                
                    <div class="mb-3">
                        <label for="name" class="form-label">Name</label>
                        <input type="text" class="form-control" name="name" value="{{old('name')}}" />
                        @error('name')
                        <span class="text text-danger">{{$message}}</span>
                        @enderror
                    </div>
                    <div class="mb-3">
                        <label for="email" class="form-label">Email</label>
                        <input type="text" class="form-control" name="email" value="{{old('email')}}" />
                        @error('email')
                        <span class="text text-danger">{{$message}}</span>
                        @enderror
                    </div>
                    <div class="mb-3">
                        <label for="password" class="form-label">Password</label>
                        <input type="password" class="form-control" name="password" />
                        @error('password')
                        <span class="text text-danger">{{$message}}</span>
                        @enderror
                    </div>
                <div class="d-flex justify-content-around align-items-center mb-4">
                    <input type="submit" class="btn btn-dark btn-lg" value="Register">
                    <a href="{{route('login')}}">Already have an account?</a>
                </div>
            </form>
        </div>
    </div>

</div>

@endsection