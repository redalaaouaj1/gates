@extends('layouts.app')
@section('content')

<form action="{{route('posts.update',$post)}}" method="post">
    @csrf
    @method('put')
    <div class="mb-3">
        <label class="form-label">Titre</label>
        <input type="text" class="form-control" name="title" placeholder="title" value="{{old('title',$post->title)}}" />
    </div>
    <div class="mb-3">
        <label class="form-label">Description</label>
        <textarea class="form-control" name="description" placeholder="description">{{old('description',$post->description)}}</textarea>
    </div>
    <input type="submit" value="Modifier" class="btn btn-light">
    <a href="{{url()->previous()}}" class="btn btn-secondary">Back</a>

</form>

@endsection