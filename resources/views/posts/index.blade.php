@extends('layouts.app')
@section('content')

@if(session('success'))
<div class="alert alert-success">
    {{session('success')}}
</div>
@endif

<a href="{{route('posts.create')}}" class="btn btn-dark">Ajouter une publication</a>
<table class="table table-dark my-3">
    <thead>
        <th>#</th>
        <th>Titre</th>
        <th>Description</th>
        <th>Publier par</th>
        <th>Date de dernier mise à jour</th>
        <th>Actions</th>
    </thead>
    <tbody>
        @foreach($posts as $index => $p)
        <tr>
            <td>{{$index+1}}</td>
            <td>{{$p->title}}</td>
            <td>{{$p->description}}</td>
            <td>{{$p->user->name}}</td>
            <td>{{$p->updated_at}}</td>
            <td>
                <a href="{{route('posts.edit',$p->id)}}" class="btn btn-warning">Modifier</a>
                <form method="post" action="{{route('posts.destroy',$p->id)}}" style="display: inline">
                    @csrf
                    @method('delete')
                    <button type="submit" class="btn btn-danger">Supprimer</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

@endsection