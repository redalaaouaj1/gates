<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>AUTH</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <style>
        body {
            background-color: #08AEEA;
        }
    </style>

</head>

<body>

    <nav class="navbar navbar-expand-lg ">
        <div class="container-fluid ">
            <div class="collapse navbar-collapse d-flex mb-3">
                <h4><a class="nav-link  p-2" href="{{route('welcome')}}">My App</a></h4>
                <a class="nav-link ms-5" href="{{route('posts.index')}}">Posts</a>
                @auth
                <div class="ms-3 navbar-nav ms-auto p-2 ">
                    <a class="nav-link active" href="#">{{auth()->user()->name}}</a>
                    <a class="nav-link badge rounded-pill text-bg-danger fs-6 p-3" href="{{route('logout')}}">Log out</a>
                </div>
                @endauth
                @guest
                <div class="ms-3 navbar-nav ms-auto p-2">
                    <a class="nav-link badge rounded-pill text-bg-primary fs-6 p-3" href="{{route('login')}}">Log in</a>
                </div>
                @endguest
            </div>
        </div>
    </nav>

    <div class="container mt-3 ">
        @yield('content')
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
</body>

</html>